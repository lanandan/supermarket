package com.ikomet.supermarket;

import android.support.v7.app.ActionBarActivity;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;


public class MainActivity extends Activity {

	TextView txt_titles;
	ImageView img_all; 
	Animation animrotate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashscreen);
      
    	img_all=(ImageView)findViewById(R.id.img_first);
    	/*animrotate = AnimationUtils.loadAnimation(getApplicationContext(),
                 R.anim.rotate);
          
    	img_all.startAnimation(animrotate);*/
        
     Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(),
                "angillaTattoo.ttf");
        txt_titles=(TextView)findViewById(R.id.txt_info);
        txt_titles.setTypeface(tf);
       
        new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				Intent loginIntent = new Intent(MainActivity.this,
						SuperActivity.class);
				loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
						| Intent.FLAG_ACTIVITY_CLEAR_TASK);
				startActivity(loginIntent);
				overridePendingTransition(R.anim.right_in, R.anim.left_out);

			}
		}, 3000);
        
    }


   
}
