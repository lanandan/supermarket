package com.ikomet.supermarket;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class SuperActivity extends Activity {

	TextView txt_signin, txt_signup;
	ImageView img_fb, img_twitter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		txt_signin = (TextView) findViewById(R.id.txt_singin);
		txt_signup = (TextView) findViewById(R.id.txt_singup);
		img_fb = (ImageView) findViewById(R.id.img_fb);
		img_twitter = (ImageView) findViewById(R.id.img_twitter);

		txt_signin.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent signin = new Intent(SuperActivity.this,
						SignInActivity.class);
				startActivity(signin);
				finish();
				overridePendingTransition(R.anim.right_in, R.anim.left_out);
			}
		});

		txt_signup.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent signup = new Intent(SuperActivity.this,
						SignUpActivity.class);
				startActivity(signup);
				finish();
				overridePendingTransition(R.anim.right_in, R.anim.left_out);
			}
		});
	}
}
